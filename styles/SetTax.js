import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 14,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  field: {
    width: 300,
    marginTop: 20,
    marginBottom: 20,
    borderColor: '#000',
    borderRadius: 3,
    borderBottomWidth: 5,
    fontSize: 60,
    textAlign: 'center',
  },
});
