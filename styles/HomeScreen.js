import { StyleSheet } from 'react-native';
import colors from '../constants/Colors';

export default StyleSheet.create({
  customBtn: {
    backgroundColor: colors.defaultColor,
    borderRadius: 0,
    borderLeftWidth: 1,
    borderLeftColor: colors.defaultColorDark,
    height: '100%',
  },
  textColor: {
    color: colors.grey,
  },
  footer: {
    padding: 10,
    backgroundColor: colors.lightGrey,
  },
  footerCol: {
    padding: 5,
  },
  budget: {
    height: 40,
    borderColor: '#dadada',
    backgroundColor: 'white',
    padding: 10,
    borderWidth: 1,
    borderRadius: 10,
    fontSize: 24,
    color: '#7B7B7B',
  },
  credit: {
    fontSize: 24,
    color: '#7B7B7B',
  },
  subTitle: {
    color: '#7B7B7B',
    textAlign: 'right',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#7B7B7B',
  },
  checkoutBtn: {
    backgroundColor: colors.defaultColor,
  },
});
