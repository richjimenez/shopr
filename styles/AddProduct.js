import { StyleSheet } from 'react-native';
import colors from '../constants/Colors';

export default StyleSheet.create({
  content: {
    padding: 15,
  },
  label: {
    marginBottom: 1,
  },
  iconBtn: {
    textAlign: 'center',
    color: colors.defaultColor,
    marginTop: 6,
  },
  btn: {
    borderRadius: 15,
    backgroundColor: colors.defaultColor,
  },
  btnTxt: {
    color: colors.grey,
  },
  field: {
    color: '#7d7d7d',
    width: '100%',
    marginTop: 5,
    marginBottom: 10,
    borderColor: '#ebebeb',
    borderRadius: 15,
    borderWidth: 1,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 10,
  },
});
