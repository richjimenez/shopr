import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  scannerContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  closeBtn: {
    position: 'absolute',
    color: 'rgba(255,255,255,0.50)',
    bottom: 20,
    right: '50%',
    transform: [{ translateX: 20 }],
  },
});
