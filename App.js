import React, { Component } from 'react';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

import { StyleSheet, Image, Button, View, Text, SafeAreaView, StatusBar } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Firebase, { FirebaseProvider } from './config/Firebase';

import Home from './screens/Home';
import Settings from './screens/Settings';

import colors from './constants/Colors';

const AppNavigator = createStackNavigator(
  {
    Home,
  },
  {
    initialRouteName: 'Home',
  }
);
const AppContainer = createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const MyDrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: Home,
    },
    Settings: {
      screen: Settings,
    },
  },
  {
    drawerPosition: 'right',
    drawerBackgroundColor: colors.defaultColor,
    drawerType: 'slide',
    hideStatusBar: true,
    contentOptions: {
      activeTintColor: colors.defaultColor,
      activeBackgroundColor: 'white',
    },
  }
);

const MyApp = createAppContainer(MyDrawerNavigator);

export default class App extends Component {
  state = { isReady: false };
  async _cacheResourcesAsync() {
    const loadFonts = Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    });
    return Promise.all(loadFonts);
  }
  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    return (
      <FirebaseProvider value={Firebase}>
        <MyApp />
      </FirebaseProvider>
    );
  }
}
