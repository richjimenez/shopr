export default {
  defaultColor: '#FDC828',
  defaultColorDark: '#dda828',
  grey: '#373737',
  lightGrey: '#FAFAFA',
};
