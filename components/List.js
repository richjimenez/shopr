import React, { Component } from 'react';
import {
  Animated,
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { SwipeListView } from 'react-native-swipe-list-view';
import currency from 'currency.js';
import styles from '../styles/List';

export default class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listViewData: Array(20)
        .fill('')
        .map((_, i) => ({ key: `${i}`, text: `item #${i}` })),
    };
    this.rowSwipeAnimatedValues = {};
    Array(20)
      .fill('')
      .forEach((_, i) => {
        this.rowSwipeAnimatedValues[`${i}`] = new Animated.Value(0);
      });
  }

  onSwipeValueChange = swipeData => {
    const { key, value } = swipeData;
  };

  closeRow(rowMap, rowKey) {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  }

  deleteRow(rowMap, rowKey) {
    this.closeRow(rowMap, rowKey);
    const newData = [...this.props.data];
    const prevIndex = this.props.data.findIndex(item => item.key === rowKey);
    newData.splice(prevIndex, 1);
    AsyncStorage.setItem('LIST', JSON.stringify(newData), err => {
      if (!err) {
        this.props.updateList();
      }
    });
  }

  render() {
    return (
      <View style={{ height: '100%' }}>
        <SwipeListView
          data={this.props.data}
          renderItem={data => (
            <TouchableHighlight
              onPress={() => console.log('You touched me')}
              style={styles.rowFront}
              underlayColor={'#FDC828'}>
              <View>
                <Text style={styles.listText}>
                  <Text style={styles.title}>{data.item.quantity} </Text>
                  {data.item.name} <Text style={styles.title}>Unitary </Text>
                  {currency(data.item.price).format(true)} <Text style={styles.title}>Total </Text>
                  {currency(data.item.subtotal).format(true)}
                </Text>
              </View>
            </TouchableHighlight>
          )}
          renderHiddenItem={(data, rowMap) => (
            <View style={styles.rowBack}>
              <Text>Left</Text>
              <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnLeft]}
                onPress={() => this.closeRow(rowMap, data.item.key)}>
                <Text>Edit</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnRight]}
                onPress={() => this.deleteRow(rowMap, data.item.key)}>
                <Ionicons name="ios-remove-circle" style={styles.icon} size={20} />
              </TouchableOpacity>
            </View>
          )}
          leftOpenValue={75}
          rightOpenValue={-150}
          previewRowKey={'0'}
          previewOpenValue={-40}
          previewOpenDelay={3000}
          onRowDidOpen={this.onRowDidOpen}
          onSwipeValueChange={this.onSwipeValueChange}
        />
      </View>
    );
  }
}
