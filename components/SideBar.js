import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

export default class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  closeDrawer = () => {
    this.props.close();
  };

  render() {
    return (
      <View style={{ width: 200, backgroundColor: 'red' }}>
        <Text> SideBar </Text>
        <Button
          onPress={() => this.closeDrawer()}
          title="Learn More"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
      </View>
    );
  }
}
