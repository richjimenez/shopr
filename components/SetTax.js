import React, { Component } from 'react';
import { Text, View, Modal, AsyncStorage, Alert, TextInput, Button } from 'react-native';
import axios from 'axios';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import styles from '../styles/SetTax';

export default class SetTax extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      tax: '8.00',
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  componentDidMount() {
    AsyncStorage.getItem('TAX', (err, tax) => {
      if (!err) {
        if (!tax) {
          this.setState({ modalVisible: true });
          Permissions.askAsync(Permissions.LOCATION)
            .then(res => res.status)
            .then(status => {
              // get the location from user device
              if (status === 'granted') {
                return Location.getCurrentPositionAsync({});
              } else {
                Alert.alert('Location denied', 'Change the permission');
              }
            })
            .then(location => {
              // get the geocode info from google maps API
              const { latitude } = location.coords;
              const { longitude } = location.coords;
              const key = 'AIzaSyDNszThrpAv36DNbgPRkG_SdDyWw2BlKx4';
              const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${key}`;
              return axios.get(url);
            })
            .then(response => {
              // format the response data from google and get the tax rate from taxJar API
              const [data] = response.data.results;
              const [zip] = data.address_components.filter(e =>
                e.types.some(type => type === 'postal_code')
              );
              const [city] = data.address_components.filter(e =>
                e.types.some(type => type === 'locality')
              );
              const url = `https://api.taxjar.com/v2/rates/?zip=${zip.long_name}&city=${city.long_name}`;
              return axios.get(url, {
                headers: {
                  Authorization: 'Bearer 41d7507f74bd6d9d0616f4a73efe4e8a',
                  'Content-Type': 'application/json',
                },
              });
            })
            .then(response => {
              // format response data from taxJar
              const { city } = response.data.rate;
              const saleTax = (response.data.rate.combined_rate * 100).toFixed(2);
              this.setState({ tax: saleTax });
            })
            .catch(err => {
              console.error(err);
            });
        } else {
          this.setState({ tax });
        }
      } else {
        console.error(err);
      }
    });
  }

  saveTax() {
    this.setState({ modalVisible: false });
    AsyncStorage.setItem('TAX', this.state.tax.toString(), err => console.log(err));
  }

  render() {
    return (
      <View>
        <Modal animationType="slide" transparent={false} visible={this.state.modalVisible}>
          <View style={styles.container}>
            <Text style={styles.subtitle}>Confirm your</Text>
            <Text style={styles.title}>TAX rate %</Text>
            <TextInput
              style={styles.field}
              onPress={this.saveTax}
              keyboardType="numeric"
              defaultValue={this.state.tax}
              onChangeText={tax => this.setState({ tax })}
              value={this.state.text}
            />
            <Button
              color="#FDC828"
              title="Save and Continue"
              accessibilityLabel="Save TAX value"
              onPress={() => this.saveTax()}
            />
          </View>
        </Modal>
      </View>
    );
  }
}
