import React, { Component } from 'react';
import { Modal, TextInput, AsyncStorage } from 'react-native';
import shortid from 'shortid';
import {
  Container,
  Header,
  Button,
  Left,
  Right,
  Body,
  Title,
  Icon,
  Content,
  Text,
} from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from '../styles/AddProduct';

export default class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      name: '',
      quantity: 1,
      price: '',
    };
  }

  toggleAddProduct() {
    this.setState(prevState => ({
      modalVisible: !prevState.modalVisible,
    }));
  }

  addQuantity = () => {
    this.setState(prevState => ({
      quantity: prevState.quantity + 1,
    }));
  };

  restQuantity = () => {
    if (this.state.quantity > 1) {
      this.setState(prevState => ({
        quantity: prevState.quantity - 1,
      }));
    }
  };

  changePrice(p) {
    const price = p.replace(/[^0-9\.]/g, '');
    this.setState({ price });
  }

  changeQuantity(q) {
    const quantity = Number(q.replace('[^0-9]', ''));
    this.setState({ quantity });
  }

  saveProduct = async () => {
    const self = this;
    // AsyncStorage.removeItem('LIST', (err, res) => {});

    try {
      let list = await AsyncStorage.getItem('LIST');
      if (list === null) list = '[]';
      const tax = await AsyncStorage.getItem('TAX');
      const listObject = JSON.parse(list);
      const product = self.state;
      delete product.modalVisible;
      product.key = shortid.generate();
      product.unitary = Number(product.price);
      product.subtotal = (Number(product.quantity) * Number(product.price)).toFixed(2);
      product.tax = (parseFloat(product.subtotal) * parseFloat(tax)) / 100;
      product.total = (Number(product.subtotal) + Number(product.tax)).toFixed(2);
      listObject.push(product);
      await AsyncStorage.setItem('LIST', JSON.stringify(listObject));
      this.setState({ modalVisible: false, quantity: 1, name: '', price: '' });
      this.props.updateList();
      this.props.getTotals();
    } catch (error) {
      // Error saving data
      console.log('Error on saving product', error);
    }
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible(!this.state.modalVisible);
        }}>
        <Container>
          <Header>
            <Left />
            <Body>
              <Title>Add Product</Title>
            </Body>
            <Right>
              <Button onPress={() => this.toggleAddProduct()} transparent>
                <Icon name="close" style={styles.btnTxt} />
              </Button>
            </Right>
          </Header>
          <Content style={styles.content}>
            <Text style={styles.label}>Name</Text>
            <TextInput
              style={styles.field}
              placeholder={'Product name'}
              placeholderTextColor="#c5c5c5"
              onChangeText={name => this.setState({ name })}
            />
            <Text style={styles.label}>Price</Text>
            <TextInput
              style={styles.field}
              keyboardType="numeric"
              placeholder={'0.00'}
              placeholderTextColor="#c5c5c5"
              onChangeText={p => this.changePrice(p)}
              value={this.state.price.toString()}
            />
            <Text style={styles.label}>Quantity</Text>
            <Grid>
              <Row>
                <Col size={10}>
                  <Ionicons
                    name="ios-remove-circle"
                    size={35}
                    style={styles.iconBtn}
                    onPress={this.restQuantity}
                  />
                </Col>
                <Col size={80}>
                  <TextInput
                    style={styles.field}
                    keyboardType="numeric"
                    placeholderTextColor="#c5c5c5"
                    defaultValue={this.state.quantity.toString()}
                    onChangeText={q => this.changeQuantity(q)}
                    value={this.state.quantity.toString()}
                  />
                </Col>
                <Col size={10}>
                  <Ionicons
                    name="md-add-circle"
                    size={35}
                    style={styles.iconBtn}
                    onPress={this.addQuantity}
                  />
                </Col>
              </Row>
            </Grid>
            <Button style={styles.btn} onPress={this.saveProduct} full>
              <Text>Add Product</Text>
            </Button>
          </Content>
        </Container>
      </Modal>
    );
  }
}
