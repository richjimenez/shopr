import React, { Component } from 'react';
import { StyleSheet, View, Image, Modal, Alert } from 'react-native';
import { Container, Content, Button, Text, Icon } from 'native-base';
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Ionicons } from '@expo/vector-icons';
import styles from '../styles/CodeScanner';
import layout from '../constants/Layout';

export default class CodeScanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      hasCameraPermission: null,
      scanned: false,
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  };

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    Alert.alert('Done', `Bar code with type ${type} and data ${data} has been scanned!`, [
      {
        text: 'OK',
        onPress: () => {
          this.setState({ scanned: false });
          this.setModalVisible(!this.state.modalVisible);
        },
      },
    ]);
  };

  toggleScanner() {
    this.setState(prevState => ({
      modalVisible: !prevState.modalVisible,
    }));
  }

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible(!this.state.modalVisible);
        }}>
        <View style={styles.scannerContainer}>
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          />

          <Image style={{ width: 300, height: 175 }} source={require('../assets/scan-zone.png')} />
        </View>
        <Ionicons
          name="ios-close-circle"
          size={50}
          style={styles.closeBtn}
          onPress={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        />
      </Modal>
    );
  }
}
