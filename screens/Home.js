import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Alert,
  Modal,
  AsyncStorage,
  Image,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import { Container, Header, Button, Left, Right, Body, Icon, Text } from 'native-base';
import currency from 'currency.js';
import { Col, Row, Grid } from 'react-native-easy-grid';

import { Ionicons } from '@expo/vector-icons';

import CodeScanner from '../components/CodeScanner';
import SetTax from '../components/SetTax';
import AddProduct from '../components/AddProduct';
import List from '../components/List';

import styles from '../styles/HomeScreen';

export default class Home extends Component {
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: ({ tintColor }) => <Ionicons name="ios-home" size={25} color={tintColor} />,
  };

  constructor(props) {
    super(props);
    this.scanner = React.createRef();
    this.addProductModal = React.createRef();
    this.state = {
      isReady: false,
      subtotal: 0,
      taxTotal: 0,
      total: 0,
      budget: null,
      credit: 0,
      list: [],
    };
  }

  componentDidMount() {
    this.updateList();
  }

  showScanner = () => {
    this.scanner.current.toggleScanner();
  };

  showAddProduct = () => {
    this.addProductModal.current.toggleAddProduct();
  };

  toggleDrawer = () => {
    this.props.navigation.toggleDrawer();
  };

  updateList = () => {
    const self = this;
    AsyncStorage.getItem('LIST', (err, res) => {
      if (!err) {
        if (res) {
          self.setState({ list: JSON.parse(res) });
          this.getTotals();
        }
      }
    });
    AsyncStorage.getItem('BUDGET', (err, res) => {
      if (!err) {
        console.log(res);
        if (res) {
          console.log(res);
          self.setState({ budget: res });
          this.getTotals();
        }
      }
    });
  };

  setBudget = budget => {
    this.setState({ budget });
    AsyncStorage.setItem('BUDGET', budget);
    this.getTotals();
  };

  getTotals = () => {
    const self = this;
    let subtotal = 0;
    let taxTotal = 0;
    let total = 0;
    this.state.list.forEach(el => {
      subtotal += Number(el.subtotal);
      taxTotal += Number(el.tax);
      total += Number(el.total);
    });
    this.setState({ subtotal });
    this.setState({ taxTotal });
    this.setState({ total });
    AsyncStorage.getItem('BUDGET', (err, res) => {
      if (!err) {
        if (res) {
          self.setState({ budget: res });
          this.setState({ credit: Number(res) - total });
        }
      }
    });
  };

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Image style={{ width: 129, height: 30 }} source={require('../assets/logo.png')} />
          </Left>
          <Body />
          <Right>
            <Button onPress={this.toggleDrawer} transparent>
              <Ionicons name="ios-menu" size={35} color="black" />
            </Button>
          </Right>
        </Header>

        <View style={{ backgroundColor: 'red', height: 40 }}>
          <Grid>
            <Row>
              <Col>
                <Button
                  iconRight
                  style={styles.customBtn}
                  onPress={this.showScanner}
                  title="Scan product">
                  <Text style={styles.textColor}>Scan product</Text>
                  <Icon style={styles.textColor} name="ios-qr-scanner" />
                </Button>
              </Col>
              <Col>
                <Button
                  iconRight
                  style={styles.customBtn}
                  onPress={this.showAddProduct}
                  title="Add product">
                  <Text style={styles.textColor}>Add product</Text>
                  <Icon style={styles.textColor} name="ios-add-circle-outline" />
                </Button>
              </Col>
            </Row>
          </Grid>
        </View>
        <KeyboardAvoidingView style={{ height: '100%' }} behavior="height" enabled>
          <Grid>
            <Row size={65}>
              <Col>
                <List data={this.state.list} updateList={this.updateList} />
              </Col>
            </Row>
            <Row size={35} style={styles.footer}>
              <Col>
                <Row style={{ height: 100 }}>
                  <Col style={styles.footerCol}>
                    <TextInput
                      style={styles.budget}
                      placeholder="Budget"
                      keyboardType="numeric"
                      returnKeyType="done"
                      onChangeText={this.setBudget}
                      value={this.state.budget}
                    />
                    <Text>Credit:</Text>
                    <Text style={styles.credit}>{currency(this.state.credit).format(true)}</Text>
                  </Col>
                  <Col style={styles.footerCol}>
                    <Text style={styles.subTitle}>
                      subtotal:{' '}
                      <Text style={styles.title}>{currency(this.state.subtotal).format(true)}</Text>
                    </Text>
                    <Text style={styles.subTitle}>
                      tax:{' '}
                      <Text style={styles.title}>{currency(this.state.taxTotal).format(true)}</Text>
                    </Text>
                    <Text style={styles.subTitle}>
                      total:{' '}
                      <Text style={styles.title}>{currency(this.state.total).format(true)}</Text>
                    </Text>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button style={styles.checkoutBtn}>
                      <Text>Checkout</Text>
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </KeyboardAvoidingView>

        {/*Modals*/}
        <SetTax />
        <CodeScanner ref={this.scanner} />
        <AddProduct
          ref={this.addProductModal}
          updateList={this.updateList}
          getTotals={this.getTotals}
        />
      </Container>
    );
  }
}
