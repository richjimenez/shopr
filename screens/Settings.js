import React, { Component } from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content,
  Text,
} from 'native-base';

import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view';
import { Ionicons } from '@expo/vector-icons';
import * as Facebook from 'expo-facebook';
import { withFirebaseHOC } from '../config/Firebase';

class Settings extends Component {
  // settings for the sidebar menu
  static navigationOptions = {
    drawerLabel: 'Settings',
    drawerIcon: ({ tintColor }) => <Ionicons name="ios-settings" size={25} color={tintColor} />,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // Listen for authentication state to change.
    this.props.firebase.checkUserAuth(user => {
      if (user != null) {
        console.log(user);
        console.log('We are authenticated now!');
      } else {
        console.log('no user auth');
      }
      // Do other things
    });
  }

  login = () => {
    Facebook.initializeAsync('2485432331566421').then(() => {
      Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile'],
      }).then(({ type, token, expires, permissions, declinedPermissions }) => {
        if (type === 'success') {
          // get the credential from firebase
          const credential = this.props.firebase.facebookAuthProviderCredential(token);
          this.props.firebase.signInWithCredential(credential);
        } else {
          // type === 'cancel'
        }
      });
    });
    // Facebook.logInWithReadPermissionsAsync('2485432331566421', {
    //   permissions: ['public_profile'],
    // }).then(result => console.log(result));
  };

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name="menu" />
            </Button>
          </Right>
        </Header>
        <Content>
          <Button onPress={this.login}>
            <Text>Facebook Login</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default withFirebaseHOC(Settings);
